<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<title>Merry</title>
<meta charset="UTF-8">
<!--Style Sheet-->
<link rel="stylesheet" type="text/css" href="Assets/css/flexslider.css" media="screen">
<link rel="stylesheet" type="text/css" href="Assets/css/sequence.css" media="screen">
<link rel="stylesheet" type="text/css" href="Assets/css/lightbox.css" media="screen">
<link rel="stylesheet" type="text/css" href="Assets/css/style.css" media="all">
<!--Special Styles-->
<link rel="stylesheet" type="text/css" href="Assets/css/style.css" title="default" media="screen">
<link rel="alternate stylesheet" type="text/css" href="Assets/css/elegant.css" title="elegant" media="screen">
<link rel="alternate stylesheet" type="text/css" href="Assets/css/modern.css" title="modern" media="screen">
<link rel="alternate stylesheet" type="text/css" href="Assets/css/colorfull.css" title="colorfull" media="screen">
<!--Responsive-->
<link rel="stylesheet" type="text/css" href="Assets/css/responsitive.css" media="all">
<!--Google fonts-->
<link href='http://fonts.googleapis.com/css?family=Russo+One&subset=latin,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
<!--Javascript-->
<script src="Assets/js/jquery-1.7.2.min.js"></script>
<!--[if lt IE9]>
<script src="js/html5.js"></script>
<script src="js/IE7.js"></script>
<![endif]-->
</head>
<body>
<header class="dark">
  <nav>
    <div id="logo">
      <h1>Merry</h1>
    </div>
    <div id="menu">
      <ul>
        <li> <a href="#slider" >Home</a></li>
        <li> <a href="#portofolio">Portofolio</a></li> 
        <li> <a href="#galery">Galery</a> </li>
        <li> <a href="#about">About</a></li>
        <li> <a href="#contact">Contact</a> </li>
      </ul>
    </div>
  </nav>
</header>
<section id="slider">
  <div id="sequence-preloader">
    <div class="preloading"> <img src="Assets/images/icons/loader.gif" alt=""> </div>
  </div>
  <div id="slideshow"> <img class="prev" src="Assets/images/prev.png" alt=""> <img class="next" src="Assets/images/next.png" alt="">
    <ul>
      <li> <img class="backgrd" src="Assets/images/bg/4.JPG" alt=""> 
      <li> <img class="backgrd" src="Assets/images/bg/2.jpg" alt=""> 
      <li> <img class="backgrd" src="Assets/images/bg/3.jpg" alt="">
      </li>
    </ul>
  </div>
</section>
<section id="portofolio" class="container dark">
  <div class="content">
    <div class="title icon-power">
      <h1>Portofolio <span>Look my Portofolio</span></h1>
      <div class="description"></div>
    </div>
    <div class="full">
      <div class="column-half">
        <p>if I want to know more you can see this website there is information that is important here, I was active in the field of journalism organization that now serves as a human resource development .I quickly bored with the things I did but I kept trying to finish my work until completed.</p>
      </div>
      <div class="column-half">
        <div class="image"><img src="Assets/images/coba.png" alt=""></div>
      </div>
    </div>
  </div>
</section>
<section id="galery" class="container light">
  <div class="content">
    <div class="title icon-preview">
      <h1>Galery <span>This is My Galery</span></h1>
      <div class="description">
        <nav id="filter"></nav>
      </div>
    </div>
    <article class="portfolio">
      <ul id="stage">
      <li data-tags="Mountain">
          <div class="thumb"><a rel="lightbox" href="Assets/images/sample/1.jpg"><img src="Assets/images/sample/1.jpg" alt=""></a></div>
          <div class="info">
            <h3>Best Frend</h3>
            <p>Kawah Ijen</p>
          </div>
        </li>
        <li data-tags="Photography">
          <div class="thumb"><a rel="lightbox" href="Assets/images/sample/1.jpg"><img src="Assets/images/sample/2.jpg" alt=""></a></div>
          <div class="info">
            <h3>Stand Alone</h3>
            <p>Strong Women</p>
          </div>
        </li>
        <li data-tags="Beach">
          <div class="thumb"><a rel="lightbox" href="Assets/images/sample/1.jpg"><img src="Assets/images/sample/3.jpg" alt=""></a></div>
          <div class="info">
            <h3>The Wild One</h3>
            <p>Illustration</p>
          </div>
        </li>
        <li data-tags="Sky">
          <div class="thumb"><a rel="lightbox" href="Assets/images/sample/1.jpg"><img src="Assets/images/sample/4.jpg" alt=""></a></div>
          <div class="info">
            <h3>Pure Beach</h3>
            <p>Boom Beach</p>
          </div>
        </li>
        <li data-tags="Design">
          <div class="thumb"><a rel="lightbox" href="Assets/images/sample/1.jpg"><img src="Assets/images/sample/5.jpg" alt=""></a></div>
          <div class="info">
            <h3>Culture</h3>
            <p>Dayak Mine</p>
          </div>
        </li>
        <li data-tags="Culture">
          <div class="thumb"><a rel="lightbox" href="Assets/images/sample/1.jpg"><img src="Assets/images/sample/6.jpg" alt=""></a></div>
          <div class="info">
            <h3>Pumkin</h3>
            <p>JatimPark</p>
          </div>
        </li>
      </ul>
    </article>
  </div>
</section>
<section id="about" class="container dark">
  <div class="content">
    <div class="title icon-and">
      <h1>About <span> Learn more about me</span></h1>
      <div class="description">
        <p>My majored in informatics Brawijaya University I'm trying to create a web</p>
      </div>
    </div>
    <div class="full">
      <div class="image"><img src="Assets/images/sample/callout.jpg" alt=""></div>
      <div class="divider"></div>
      <div class="column-half">
        <div class="subtitle">
          <h3>Short Story:</h3>
        </div>
        <p>selecting indeed very difficult, I must make a choice to go to college in the field that I did not really interested .Yes this is a very appropriate choice Brawijaya University informatics techniques sometimes improper choice can make you a more useful and growing, despite the many challenges faced.  but hang in there because when you are confident and believe you can pass even more than others I sometimes feel inadequate and incapable but I also have a God who is always there for me under any circumstances</p>
      </div>
      <div class="column-half last">
        <div class="subtitle">
          <h3>Skills:</h3>
        </div>
        <ul id="skills">
          <li><span title="80"></span>
            <p>Html <strong>80%</strong></p>
          </li>
          <li><span title="83"></span>
            <p>PHP <strong>83%</strong></p>
          </li>
          <li><span title="60"></span>
            <p>Codeigniter(CI)<strong>60%</strong></p>
          </li>
          <li><span title="70"></span>
            <p>CSS <strong>70%</strong></p>
          </li>
          <li><span title="50"></span>
            <p>Bootstrap<strong>50%</strong></p>
          </li>
           <li><span title="80"></span>
            <p>Speak English <strong>80%</strong></p>
          </li>
        </ul>
        <!--END skills-->
      </div>
      <div class="column-half">
        <div class="subtitle">
          <h3>Education:</h3>
        </div>
        <div class="accordion" id="list1">
          <!--Accordion-->
          <a>Primary School Sirpang Dalik Raya 2001</a>
          <div>
            <p>keep the spirit and continue to fight to the bitter end and do your best for your life.</p>
          </div>
          <a>Junior High School 1 Raya 2007</a>
          <div>
            <p>keep the spirit and continue to fight to the bitter end and do your best for your life.</p>
          </div>
          <a>Senior High School 1 Raya 2010</a>
          <div>
            <p>keep the spirit and continue to fight to the bitter end and do your best for your life.</p>
          </div>
          <a>Brawijaya University 2013</a>
          <div>
            <p>keep the spirit and continue to fight to the bitter end and do your best for your life.</p>
          </div>
             </div>
        </div>
        </div>
        <!-- end tab container -->
      </div>
</section>
<section id="contact" class="container light">
  <div class="content">
    <div class="title icon-phone">
      <h1>Contact <span>What we offer</span></h1>
      <div class="description"> <a href="https://www.instagram.com/"><img src="Assets/images/icons/instagram.png" alt="" class="social"></a> <a href="https://www.facebook.com/"><img src="Assets/images/icons/fb.png" alt="" class="social"></a> <a href="https://twitter.com/allnababan"><img src="Assets/images/icons/tw.jpg" alt="" class="social"></a></div>
    </div>
    <div class="full">
      <div id="contact-form">
        <form id="contact-us" action="#">
          <div class="column-half">
            <div class="formblock">
              <input type="text" name="contactName" id="contactName" value="" class="txt requiredField" placeholder="Name:">
            </div>
            <div class="formblock">
              <input type="text" name="email" id="email" value="" class="txt requiredField email" placeholder="Email:">
            </div>
            <div class="formblock">
              <input type="text" name="website" id="website" value="" class="txt website" placeholder="Website:">
            </div>
            <div class="formblock">
              <textarea name="comments" id="commentsText" class="txtarea requiredField" placeholder="Message:"></textarea>
            </div>
            <button name="submit" type="submit" class="subbutton"></button>
          </div>
          <div class="column-half last">
            <div id="Blog"></div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<footer> <a href="http://three-merrynbn.rhcloud.com/" class="blog"> <span class="blog"></span>
  <p class="blog_status"></p>
  </a>
  <section class="footer">
    <p>&copy; Copyright 2016 All Rights Reserved<a target="_blank" href="#">Merryg</a></p>
  </section>
</footer>
<!--Javascript-->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="Assets/js/jquery.flexslider-min.js"></script>
<script src="Assets/js/sequence.jquery-min.js"></script>
<script src="Assets/js/waypoints.min.js"></script>
<script src="Assets/js/quicksand.js"></script>
<script src="Assets/js/jquery.masonery.js"></script>
<script src="Assets/js/modernizr.custom.js"></script>
<script src="Assets/js/gmaps.js"></script>
<script src="Assets/js/jquery.accordion.js"></script>
<script src="Assets/js/lightbox.js"></script>
<script src="Assets/js/styleswitch.js"></script>
<script src="Assets/js/custom.js"></script>
<!--Sequence slider-->
<script>
var options = {
    autoPlay: true,
    nextButton: ".next",
    prevButton: ".prev",
    preloader: "#sequence-preloader",
    prependPreloader: false,
    prependPreloadingComplete: "#sequence-preloader, #slideshow",
    animateStartingFrameIn: true,
    transitionThreshold: 500,
    nextButtonAlt: " ",
    prevButtonAlt: " ",
    afterPreload: function () {
        $(".prev, .next").fadeIn(500);
        if (!slideShow.transitionsSupported) {
            $("#slideshow").animate({
                "opacity": "1"
            }, 1000);
        }
    }
};
var slideShow = $("#slideshow").sequence(options).data("sequence");
if (!slideShow.transitionsSupported || slideShow.prefix == "-o-") {
    $("#slideshow").animate({
        "opacity": "1"
    }, 1000);
    slideShow.preloaderFallback();
}
</script>
<script>
new GMaps({
    div: '#map',
    lat: 44.79913,
    lng: 20.47662
});
</script>
<script>
if (typeof jQuery == 'undefined') {
    document.write(unescape("%3Cscript src='Assets/js/jquery.js'%3E%3C/script%3E"));
}
</script>
</body>
</html>
